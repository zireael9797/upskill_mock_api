package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type SubDomain struct {
	Id       string `json:"id"`
	DomainId string `json:"domain_id"`
	Title    string `json:"title"`
	Image    string `json:"image"`
}

// dummy subdomain data
var subDomains = []SubDomain{
	{
		Id:       "8765",
		DomainId: "1234", // For Programming domain
		Title:    "Front End",
		Image:    "https://i.ibb.co/pb9Kppv/frontend.jpg",
	},
	{
		Id:       "4321",
		DomainId: "1234", // For Programming domain
		Title:    "Back End",
		Image:    "https://i.ibb.co/pb9Kppv/frontend.jpg", //too lazy to make an actual backend image lol
	},
	{
		Id:       "1569",
		DomainId: "5678", // For Other domain
		Title:    "Bla bla",
		Image:    "https://i.ibb.co/pb9Kppv/frontend.jpg", //too lazy to make an actual backend image lol
	},
}

func GetSubDomains() gin.HandlerFunc {
	//--------------------STATUS OK 200 for subdomains.
	return func(c *gin.Context) {
		domainID := c.Param("did")
		filtered := make([]SubDomain, 0)
		for _, subdomain := range subDomains {
			if subdomain.DomainId == domainID {
				filtered = append(filtered, subdomain)
			}

		}
		//this json method will automatically convert the struct/structs_list to json
		//will convert all the variable names too
		//cool shit
		//also works on maps too, not just structs
		c.JSON(http.StatusOK, filtered)
	}
}
