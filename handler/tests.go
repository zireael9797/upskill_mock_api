package handler

import (
	"fmt"
	"main/database"
	"net/http"

	"github.com/gin-gonic/gin"
)

//------------------------------Need STATUS OK
//------------------------------Ignore the fake DB Stuff. It's not a real database. Just contains 2 hardcoded questions
//------------------------------Return only ONE random test for the provided TOPIC ID :tid
func GetTest() gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("tid")
		fmt.Println(c.Param("tid"))
		tests := database.GetTests()
		found := false
		for _, test := range tests.Tests {
			if test.TopicId == id {
				c.JSON(http.StatusOK, test)
				found = true
				break
			}
		}
		if !found {
			c.JSON(http.StatusNotFound, gin.H{"error": "Item not found"})
		}
	}
}

//-----------------------For contributor to post a new TEST
//-----------------------Save in DB and then send a StatusCreated code
func SubmitTest() gin.HandlerFunc {
	return func(c *gin.Context) {
		var test database.TestModel
		err := c.ShouldBindJSON(&test)
		if err != nil {
			fmt.Println(err)
			c.JSON(http.StatusBadRequest, gin.H{

				"message": err.Error(),
			})
			return
		}
		c.Status(http.StatusCreated)
	}
}

//---------------NOT NEEDED NOW
func GetTests() gin.HandlerFunc {
	return func(c *gin.Context) {
		tests := database.GetTests()
		c.JSON(http.StatusOK, tests.Tests)
	}
}
