package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type SubmissionModel struct {
	Correct float64 `json:"correct"`
	Total   float64 `json:"total"`
	TestId  string  `json:"test_id"`
	Token   string  `json:"token"`
}

func SubmitResult() gin.HandlerFunc {
	return func(c *gin.Context) {
		var submission SubmissionModel
		c.ShouldBindJSON(&submission)
		submission.Token = c.GetHeader("token")
		if len(submission.Token) > 0 {
			c.Status(http.StatusCreated)
			return
		} else {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"message": "invalid data"})
			return
		}
	}
}
