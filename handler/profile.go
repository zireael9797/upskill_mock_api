package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Stat struct {
	Title     string `json:"title"`
	Image     string `json:"image"`
	Created   string `json:"created"`
	Domain    string `json:"coding"`
	View      int    `json:"view"`
	Completed int    `json:"completed"`
}

var stats []Stat

func GetProfile() gin.HandlerFunc {
	return func(c *gin.Context) {
		stats = append(stats, Stat{
			Title:     "Vue",
			Image:     "https://i.ibb.co/54wVWbW/vue.jpg",
			Created:   "20/01/2020",
			Domain:    "Coding",
			View:      213,
			Completed: 156,
		})
		stats = append(stats, Stat{
			Title:     "NodeJS",
			Image:     "https://i.ibb.co/54wVWbW/vue.jpg",
			Created:   "20/01/2020",
			Domain:    "Coding",
			View:      213,
			Completed: 156,
		})
		stats = append(stats, Stat{
			Title:     "Angular",
			Image:     "https://i.ibb.co/54wVWbW/vue.jpg",
			Created:   "20/01/2020",
			Domain:    "Coding",
			View:      213,
			Completed: 156,
		})

		c.JSON(http.StatusOK, gin.H{
			"name":  "Eugene Bailey",
			"image": "https://i.ibb.co/L5VjC6G/dp.jpg",
			"stats": stats,
			"result": gin.H{
				"nodejs": []int{
					30,
					26,
				},
				"NodeJS": []int{
					30,
					15,
				},
				"Angular": []int{
					30,
					20,
				},
			},
			"reward": gin.H{
				"20pt":  5,
				"100pt": 15,
			},
		},
		)
	}
}
