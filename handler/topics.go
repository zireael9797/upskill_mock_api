package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Topic struct {
	Id          string `json:"id"`
	SubdomainId string `json:"subdomain_id"`
	Title       string `json:"title"`
	Image       string `json:"image"`
	Time        int    `json:"time"`
	Questions   int    `json:"questions"`
}

var topics = []Topic{
	Topic{
		Id:          "1459",
		SubdomainId: "8765",
		Title:       "Angular",
		Image:       "https://i.ibb.co/54wVWbW/vue.jpg",
		Time:        30,
		Questions:   20,
	},
	Topic{
		Id:          "3693",
		SubdomainId: "8765",
		Title:       "Vue",
		Image:       "https://i.ibb.co/54wVWbW/vue.jpg",
		Time:        30,
		Questions:   20,
	},
}

func GetTopics() gin.HandlerFunc {
	//--------------------STATUS OK 200 for topic.
	//--------------------This MOCK API returns all topics regardles of SUBDOMAIN ID
	//--------------------In real use case, must search a database for subdomains of the provided domain :sdid
	return func(c *gin.Context) {
		c.JSON(http.StatusOK, topics)
	}
}
