package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Domain struct {
	Id    string `json:"id"`
	Title string `json:"title"`
	Image string `json:"image"`
}

// dummy domain data
var domains = []Domain{
	{
		Id:    "5678",
		Title: "Digital Marketing",
		Image: "https://i.ibb.co/7XK66Sr/marketing.jpg",
	},
	{
		Id:    "1234",
		Title: "Programming",
		Image: "https://i.ibb.co/5hPfWJJ/code.jpg",
	},
}

func GetDomains() gin.HandlerFunc {
	//-----------------------STATUS OK (200) for domains
	return func(c *gin.Context) {
		c.JSON(http.StatusOK, domains)
	}
}
