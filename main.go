package main

import (
	"main/handler"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	//This api is for a quiz app
	// - Domains are like Computer Science, Business, Finance etc
	// - Subdomains are under domain, example Computer Science > [Front End, Back End, AI etc]
	// - Topics are under subdomain, example Front End > [Flutter, Vue, React]
	// - Tests (quizzes) are under Topic, example Flutter > [Beginner, Intermediate, Expert]
	router.GET("/domains", handler.GetDomains())            // domain is top level, so doesn't need any filtering
	router.GET("/subdomains/:did", handler.GetSubDomains()) // :did is domain id, like "www.api.com/subdomains/1234", here 1234 is the domain's id (Like Computer Science), this will return all the subdomains for the COmputer Science domain
	router.GET("/topics/:sdid", handler.GetTopics())        // :sdid is the subdomain id, ....
	router.GET("/tests/:tid", handler.GetTest())            // :tid is the topic id, ....

	// After test is done the result is submitted to this endpoint
	router.POST("/submit-result", handler.SubmitResult())

	// This is an AVG of the users all time results, Sort of like `USER STATS`
	router.GET("/result", handler.GetResult())

	// We had a system to allow users to contribute new quizzes too! this is the endpoint for submitting a new test
	router.POST("/submit-test", handler.SubmitTest())

	// User Profile
	router.GET("/contributor", handler.GetProfile())

	//----------AUTH
	router.POST("/signup", handler.SignupWithEmailPass())
	router.POST("/login", handler.LoginWithEmailPass())

	router.Run()

	// Look at handlers/subdomains.go for more
	// Look at handlers/login.go for more
}
