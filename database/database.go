package database

type TestModel struct {
	Id            string          `json:"id,omitempty"`
	TopicId       string          `json:"topic_id,omitempty"`
	ContributorId string          `json:"contributor_id"`
	Questions     []QuestionModel `json:"questions,omitempty"`
	Limit         int             `json:"limit,omitempty"`
}

type QuestionModel struct {
	Question      string   `json:"question,omitempty"`
	Answers       []string `json:"answers,omitempty"`
	CorrectAnswer int      `json:"correct_answer"`
}

type TestCollection struct {
	Tests []TestModel `json:"tests,omitempty"`
}

func GetTests() TestCollection {
	tests := TestCollection{
		Tests: []TestModel{test1, test2},
	}

	return tests
}

//------------------------------------THE TESTS
var test1 = TestModel{
	TopicId:       "1459",
	ContributorId: "bhjabdjaj",
	Id:            "az2xc34",
	Questions: []QuestionModel{
		QuestionModel{
			Question: "What is the capital of Spain",
			Answers: []string{
				"Berlin",
				"Buenos Aires",
				"Madrid",
				"San Juan",
			},
			CorrectAnswer: 4,
		},
		QuestionModel{
			Question: "How hot is the surface of the Sun",
			Answers: []string{
				"1,233 K",
				"5,778 K",
				"12,130 K",
				"101,300 K",
			},
			CorrectAnswer: 4,
		},
	},
	Limit: 30,
}

var test2 = TestModel{
	Id:            "hd8br92",
	TopicId:       "1459",
	ContributorId: "bhwdbajwdaj",
	Questions: []QuestionModel{
		QuestionModel{
			Question: "How tall is Mount Everest?",
			Answers: []string{
				"6,683 ft (2,037 m)",
				"7,918 ft (2,413 m)",
				"19,341 ft (5,895 m)",
				"29,029 ft (8,847 m)",
			},
			CorrectAnswer: 1,
		},
		QuestionModel{
			Question: "What is 48,879 in hexidecimal?",
			Answers: []string{
				"0x18C1",
				"0xBEEF",
				"0xDEAD",
				"0x12D591",
			},
			CorrectAnswer: 3,
		},
	},
	Limit: 30,
}
