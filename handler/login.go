package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

//A real hardcoded token looking string
var token string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnYnJsc25jaHMiLCJzdWIiOiI1ZTZmYWNiMTkwODY4NTA3OTRlMTE0M2EiLCJhdWQiOlsiaHR0cHM6Ly9nb2xhbmcub3JnIiwiaHR0cHM6Ly9qd3QuaW8iXSwiZXhwIjoxNjE1NjMwNDAxLCJuYmYiOjE1ODQ1MjgyMDEsImlhdCI6MTU4NDUyNjQwMSwianRpIjoiZm9vYmFyIn0.xEVlQBTXJuIMIL6io7rFSSZyocuIA0Qj4RSxF8kcQ-M uid:5e6facb19086850794e1143a"

type LoginModel struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func LoginWithEmailPass() gin.HandlerFunc {
	return func(c *gin.Context) {
		var login LoginModel
		c.ShouldBindJSON(&login) //should bind json will attempt to match a json object to a GoLang Struct, cool shit ha?
		if len(login.Password) > 0 && len(login.Email) > 0 {
			//---------------"email: nonexisting@nonexisting.com,password: wrongpass" are used to simulate failed login
			//---------------all other emails are valid in this MOCK API
			if login.Email != "nonexisting@nonexisting.com" {
				if login.Password != "wrongpass" {
					fmt.Println(login.Email)
					fmt.Println(login.Password)
					//-------------------------For login I need a (200) StatusOK response for success
					c.JSON(http.StatusOK, gin.H{
						"token":       token,
						"username":    "username",
						"contributor": true,
					})
				} else {
					//----------------------Make sure any kind of failed request for anything has a "message:" field with a
					//----------------------human readable message. this is true for all kinds of requests.
					c.JSON(http.StatusUnauthorized, gin.H{ //gin.H is just shorthand for map[string]Inteface{}, will take any String(Key) Any(Value) pair
						"message": "wrong email or password",
					})
				}
			} else {
				c.JSON(http.StatusNotFound, gin.H{
					"message": "wrong email or password",
				})
			}
		} else {
			c.JSON(http.StatusUnprocessableEntity, gin.H{
				"message": "you must provide email and password",
			})
		}
	}
}
