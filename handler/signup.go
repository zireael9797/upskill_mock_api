package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type SignupModel struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Username string `json:"username"`
}

func SignupWithEmailPass() gin.HandlerFunc {
	return func(c *gin.Context) {
		var signup SignupModel
		c.ShouldBindJSON(&signup)
		if len(signup.Username) > 0 && len(signup.Password) > 0 && len(signup.Email) > 0 {
			//--------------------existing@existing.com is made to simulate a signup failure. every other email is valid
			//--------------------All other emails are valid for signup in this MOCK API
			if signup.Email != "existing@existing.com" {
				fmt.Println(signup.Email)
				fmt.Println(signup.Password)
				//----------------------------------For signup I need a status created (201) response for success
				c.JSON(http.StatusCreated, gin.H{
					"token":       token, //------------------In the login handler file
					"username":    signup.Username,
					"contributor": false,
				})
			} else {
				//----------------------Make sure any kind of failed request for anything has a "message:" field with a
				//----------------------human readable message. this is true for all kinds of requests.
				c.JSON(http.StatusUnprocessableEntity, gin.H{
					"message": "email needs to be unique",
				})
			}
		} else {
			//----------------------Make sure any kind of failed request for anything has a "message:" field with a
			//----------------------human readable message. this is true for all kinds of requests.
			c.JSON(http.StatusUnprocessableEntity, gin.H{
				"message": "you must provide email, username and password",
			})
		}
	}
}
