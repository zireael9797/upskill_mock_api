package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetResult() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.JSON(http.StatusOK, gin.H{
			"avg": 70,
			"chart": gin.H{
				"node":    2,
				"vue":     3,
				"angular": 1,
			},
			"report": gin.H{
				"node": []int{
					30,
					26,
				},
				"angular": []int{
					30,
					15,
				},
				"vue": []int{
					30,
					20,
				},
			},
			"analysis": gin.H{
				"score":     8.5,
				"avg_score": 16,
			},
			"rank": []int{
				21,
				81,
			},
		},
		)
	}
}
